package ru.vmaksimenkov.tm.exception.empty;

import ru.vmaksimenkov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
